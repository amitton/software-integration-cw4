\subsection*{1 - Slicer path planning}
To validate the results of the path planning algorithm, the entry and target points of the optimum calculated trajectory could be visualised in Slicer. The results can be seen below in Figure \ref{optimum-points}. Figure \ref{fig:hippo-targeted} shows that the optimum calculated target point is situated within the hippocampus as required. Figure \ref{fig:opt-traj-with-crit-structures} shows the surrounding critical structures of the brain, and we can see that the optimum entry point is situated just above the surface. 

\begin{figure}[h]

\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=5cm]{images/hippo-targeted}
\caption{Hippocampus successfully targeted}
\label{fig:hippo-targeted}
\end{subfigure}
\hfill
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=5cm]{images/opt-traj-with-crit-structures}
\caption{Entry point in relation to critical structures }
\label{fig:opt-traj-with-crit-structures}
\end{subfigure}
\caption{Optimum trajectory visualisation}
\label{optimum-points}

\end{figure}

\subsection*{2 - OpenIGTLink for data transfer from 3D Slicer to ROS} 

\subsubsection*{Performing the transform}
The transformed optimum trajectory entry and target points were visualised in Slicer to verify the transformation (Figure \ref{fig:traj-transform}). The coordinates of the points could be checked in the Slicer \textit{Data} tab (Figure \ref{fig:traj-transform-coords}), and by comparing them with the untransformed coordinates in Figure \ref{fig:path-planning-results} we can see that the expected transformation has been successfully carried out. 
 
\begin{figure}[h]

\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=5cm]{images/traj-transform} 
\caption{Transformed points (right)}
\label{fig:traj-transform}
\end{subfigure}
\hfill
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=3cm]{images/traj-transform-coords} 
\caption{Coordinates of transformed points}
\label{fig:traj-transform-coords}
\end{subfigure}
\caption{Transformed optimum trajectory entry and target points}
\label{transformed-points}

\end{figure}

\subsubsection*{Establishing the bridge}
\emph{Note - Again, at time of submission I could not establish the TCP link using OpenIGTLink without errors being raised. What follows in this section is a description of how I would have validated the TCP link and the corresponding data transfer}.

To establish a successful network link, the network status in the OpenIGTLinkIF extension in Slicer could have been checked. Before running the \textit{bridge.launch} file, the status in Slicer would be displayed as ``WAIT'' (as seen below in Figure \ref{tcplinks}). After running the file and establishing a successful bridge, this status indicator would have changed to "ON". 

\begin{figure}[h]
\centering
\includegraphics[width=0.9\linewidth, height=3cm]{images/slicer-tcplink-1} 
\caption{OpenIGTLink Status}
\label{tcplinks}
\end{figure}

\subsubsection*{Transferring the data from Slicer}
To verify that the correct data had been sent from Slicer across the network bridge, several functions could have been written. They would have included a function to log the transform data from Slicer using \textit{rospy.loginfo()}, saving the desired x, y and z coordinates. A second function would have been made to implement the first; initialising a listener node and then calling \textit{rospy.Subscriber} to receive the resulting coordinates.  These values could then have been printed to the terminal and inspected against the \textit{BestTrajTransformed} markers sent from Slicer to check for correspondence. 

\subsection*{3 - ROS implementation to move robot}
\subsubsection*{Robot model}
To verify the robot description and \textit{moveit} packages had downloaded correctly, the robot was visualised in RViz by running the \textit{demo.launch} file inside the \textit{panda$\_$moveit$\_$config} package. From Figure \ref{robot-arm} below, we can see that the robot arm was successfully loaded into the RViz workspace, and the user was able to click and drag the end effector to change the configuration of the robot.

\begin{figure}[h]

\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=5cm]{images/robot-neutral} 
\caption{Initial configuration}
\label{fig:robot-neutral}
\end{subfigure}
\hfill
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=5cm]{images/robot-moved} 
\caption{Updated configuration}
\label{fig:robot-moved}
\end{subfigure}
\caption{Panda Arm in RViz}
\label{robot-arm}
\end{figure}


\subsubsection*{Movement controller}
The first aspect of the movement controller to be validated was the first call to the function \textit{go$\_$to$\_$joint$\_$state()}. If successful, this would show the Panda Arm in its neutral configuration in RViz and the message ``Neutral configuration reached'' at the command line. These results can be seen in Figure \ref{robot-arm-neutral}

\begin{figure}[h]
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=2.5cm]{images/robot-neutral-valid-terminal} 
\caption{Command Line}
\label{fig:robot-neutral-valid-terminal}
\end{subfigure}
\hfill
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=5cm]{images/robot-neutral-valid-rviz} 
\caption{RViz Visualisation}
\label{fig:robot-neutral-valid-rviz}
\end{subfigure}
\caption{Step 1 - Reaching `neutral' position}
\label{robot-arm-neutral}
\end{figure}

Secondly, the functionality of \textit{go$\_$to$\_$pose$\_$goal(x,y,z)} was tested. The coordinates corresponding to the points from \textit{BestTrajTransformed} in Slicer were entered at the command line as prompted, and the Panda Arm's configuration could be seen to update accordingly, as shown in \ref{robot-moved-valid}.

\begin{figure}[h]
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=2cm]{images/robot-moved-valid-terminal} 
\caption{Command Line}
\label{fig:robot-moved-valid-terminal}
\end{subfigure}
\hfill
\begin{subfigure}{0.5\textwidth}
\centering
\includegraphics[width=0.9\linewidth, height=5cm]{images/robot-moved-valid-rviz} 
\caption{RViz Visualisation}
\label{fig:robot-moved-valid-rviz}
\end{subfigure}
\caption{Step 2 - Reaching desired position}
\label{robot-moved-valid}
\end{figure}

Lastly, position validation was carried out to ensure that the desired end effector position corresponded with the resulting end effector position. The desired position was printed to the command line, where it could be seen that the Slicer to ROS (millimetre to metre) conversion had been carried out successfully. Secondly, the current position of the end effector was displayed by calling the \textit{get$\_$current$\_$pose()} function of the \textit{MoveGroupCommander} object. By inspecting the results, it could be seen that the desired and current positions were equal in all cases to at least 4 significant figures. The results of this test are shown below in Figure 8.

\begin{figure}[h]
\includegraphics[scale=0.5]{images/robot-valid-comparison}
\centering
\captionsetup{justification=centering,margin=2cm}
\caption{Position validation}
\label{fig:robot-points-comparison}
\end{figure}