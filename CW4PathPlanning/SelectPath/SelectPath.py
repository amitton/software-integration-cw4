import os
import unittest
import math
import numpy
import logging
import slicer, vtk, qt, ctk, random
import sitkUtils as su
import SimpleITK as sitk
from slicer.ScriptedLoadableModule import *


#
# SelectPath
#

class SelectPath(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "SelectPath" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["Alex Mitton (KCL)"] # replace with "Firstname Lastname (Organization)"
    self.parent.acknowledgementText = """
This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
""" # replace with organization, grant and thanks.

#
# SelectPathWidget
#

class SelectPathWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Instantiate and connect widgets ...

    #
    # Parameters Area
    #
    parametersCollapsibleButton = ctk.ctkCollapsibleButton()
    parametersCollapsibleButton.text = "Parameters"
    self.layout.addWidget(parametersCollapsibleButton)

    # Layout within the dummy collapsible button
    parametersFormLayout = qt.QFormLayout(parametersCollapsibleButton)

    #
    # input volume selector
    #
    self.TargetStructureSelector = slicer.qMRMLNodeComboBox()
    self.TargetStructureSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode"]
    self.TargetStructureSelector.selectNodeUponCreation = True
    self.TargetStructureSelector.addEnabled = False
    self.TargetStructureSelector.removeEnabled = False
    self.TargetStructureSelector.noneEnabled = False
    self.TargetStructureSelector.showHidden = False
    self.TargetStructureSelector.showChildNodeTypes = False
    self.TargetStructureSelector.setMRMLScene( slicer.mrmlScene )
    self.TargetStructureSelector.setToolTip( "Select the target structure:" )
    parametersFormLayout.addRow("Target: ", self.TargetStructureSelector)

    #
    # output volume selector
    #
    self.CriticalStructureSelector = slicer.qMRMLNodeComboBox()
    self.CriticalStructureSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode"]
    self.CriticalStructureSelector.selectNodeUponCreation = True
    self.CriticalStructureSelector.addEnabled = False
    self.CriticalStructureSelector.removeEnabled = False
    self.CriticalStructureSelector.noneEnabled = False
    self.CriticalStructureSelector.showHidden = False
    self.CriticalStructureSelector.showChildNodeTypes = False
    self.CriticalStructureSelector.setMRMLScene( slicer.mrmlScene )
    self.CriticalStructureSelector.setToolTip( "Select the critical structure to avoid:" )
    parametersFormLayout.addRow("Critical structure: ", self.CriticalStructureSelector)

    #
    # entry points selector
    #
    self.EntriesSelector = slicer.qMRMLNodeComboBox()
    self.EntriesSelector.nodeTypes = ["vtkMRMLMarkupsFiducialNode"]
    self.EntriesSelector.selectNodeUponCreation = True
    self.EntriesSelector.addEnabled = False
    self.EntriesSelector.removeEnabled = False
    self.EntriesSelector.noneEnabled = False
    self.EntriesSelector.showHidden = False
    self.EntriesSelector.showChildNodeTypes = False
    self.EntriesSelector.setMRMLScene(slicer.mrmlScene)
    self.EntriesSelector.setToolTip("Select the possible entry points:")
    parametersFormLayout.addRow("Entry points: ", self.EntriesSelector)

    #
    # target points selector
    #
    self.TargetsSelector = slicer.qMRMLNodeComboBox()
    self.TargetsSelector.nodeTypes = ["vtkMRMLMarkupsFiducialNode"]
    self.TargetsSelector.selectNodeUponCreation = True
    self.TargetsSelector.addEnabled = False
    self.TargetsSelector.removeEnabled = False
    self.TargetsSelector.noneEnabled = False
    self.TargetsSelector.showHidden = False
    self.TargetsSelector.showChildNodeTypes = False
    self.TargetsSelector.setMRMLScene(slicer.mrmlScene)
    self.TargetsSelector.setToolTip("Select the possible target points:")
    parametersFormLayout.addRow("Target points: ", self.TargetsSelector)

    #
    # Apply Button
    #
    self.RunButton = qt.QPushButton("Run")
    self.RunButton.toolTip = "Run the algorithm."
    self.RunButton.enabled = False
    parametersFormLayout.addRow(self.RunButton)

    # connections
    self.RunButton.connect('clicked(bool)', self.onApplyButton)
    self.TargetStructureSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)
    
    # Add vertical spacer
    self.layout.addStretch(1)

    # Refresh Apply button state
    self.onSelect()

  def cleanup(self):
    pass

  def onSelect(self):
    self.RunButton.enabled = self.TargetStructureSelector.currentNode() and self.CriticalStructureSelector.currentNode() and self.EntriesSelector.currentNode() and self.TargetsSelector.currentNode()
    

  def onApplyButton(self):
    logic = SelectPathLogic()
    logic.run(self.TargetStructureSelector.currentNode(), self.CriticalStructureSelector.currentNode(),
              self.EntriesSelector.currentNode(),self.TargetsSelector.currentNode())

#
# SelectPathLogic
#

class SelectPathLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def hasImageData(self,volumeNode):
    """This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not volumeNode:
      logging.debug('hasImageData failed: no volume node')
      return False
    if volumeNode.GetImageData() is None:
      logging.debug('hasImageData failed: no image data in volume node')
      return False
    return True



  def run(self, targetStructure, criticalStructure, entries, targets):
    """
    Run the actual algorithm
    """
    print
    print('-'*20)
    logging.info('Processing started')
    print('-'*20)
    print

    print 'Total inital number of possible trajectories =',entries.GetNumberOfFiducials()*targets.GetNumberOfFiducials()
    print

    goodTargets = self.comparePoints(targetStructure,targets)

    print 'Number of target points in target structure =',goodTargets.GetNumberOfFiducials()
    print
    print 'Number of trajectories targetting structure =', goodTargets.GetNumberOfFiducials()*entries.GetNumberOfFiducials()
    print
    
    goodTrajEntry, goodTrajTarget = self.findGoodTraj(criticalStructure, entries, goodTargets, 50)

    print 'Number of good trajectories avoiding critical structure =',len(goodTrajEntry)
    print
          
    bestTrajEntry, bestTrajTarget, bestTrajDistance = self.findBestTraj(criticalStructure,goodTrajEntry,goodTrajTarget)

    print 'Optimised Trajectory: '
    print'- Entry point =', bestTrajEntry
    print'- Target point =', bestTrajTarget
    print'- Distance value =', bestTrajDistance
    print

    print('-'*20)
    logging.info('Processing completed')
    print('-'*20)    

    return True


  def comparePoints(self,targetStructure,targets):

    ijkMat = vtk.vtkMatrix4x4()
    targetStructure.GetRASToIJKMatrix(ijkMat)
    trans = vtk.vtkTransform()
    trans.SetMatrix(ijkMat)

    goodTargets = slicer.mrmlScene.AddNode(slicer.vtkMRMLMarkupsFiducialNode())
    goodTargets.SetName('GoodTargets')

    for x in range(0,targets.GetNumberOfFiducials()):
      fidCoords = [0,0,0]
      targets.GetNthFiducialPosition(x,fidCoords)
      index = trans.TransformPoint(fidCoords)
      imageCoords = targetStructure.GetImageData().GetScalarComponentAsDouble(int(index[0]),int(index[1]),int(index[2]),0)
      if imageCoords == 1:
        goodTargets.AddFiducial(fidCoords[0],fidCoords[1],fidCoords[2])

    return goodTargets


  def findGoodTraj(self,criticalStructure,entries,targets,lengthThreshold):

    goodEntry = []
    goodTarget = []

    obb_tree = self.obbTree(criticalStructure)

    for i in range(0, entries.GetNumberOfFiducials()):
      entryPos = [0,0,0]
      entries.GetNthFiducialPosition(i,entryPos)

      for j in range(0, targets.GetNumberOfFiducials()):
        targetPos = [0,0,0]
        targets.GetNthFiducialPosition(j,targetPos)

        if not obb_tree.IntersectWithLine(entryPos,targetPos,vtk.vtkPoints(),vtk.vtkIdList()):

          trajLength = math.sqrt(((targetPos[0]-entryPos[0])**2)
                       +((targetPos[1]-entryPos[1])**2)
                       +((targetPos[2]-entryPos[2])**2))

          if trajLength < lengthThreshold:
          
            goodEntry.append(entryPos)
            goodTarget.append(targetPos)

    return goodEntry, goodTarget
  

  def findBestTraj(self,criticalStructure,goodEntry,goodTarget):

    sitkInput = su.PullVolumeFromSlicer(criticalStructure)
    distanceFilter = sitk.DanielssonDistanceMapImageFilter()
    sitkOutput = distanceFilter.Execute(sitkInput)
    distMap = su.PushVolumeToSlicer(sitkOutput, None, 'distanceMap')

    transform = vtk.vtkTransform()
    transformMatrix = vtk.vtkMatrix4x4()
    distMap.GetRASToIJKMatrix(transformMatrix)
    transform.SetMatrix(transformMatrix)

    sampleNo = 20
    bestTrajDistance = 0
    bestTrajEntry = []
    bestTrajTarget = []

    for trajNo in range(0,len(goodEntry)):

      trajEntry = goodEntry[trajNo]
      trajTarget = goodTarget[trajNo]
      trajDistanceVal = 0

      xCoords = numpy.linspace(trajEntry[0], trajTarget[0], sampleNo)
      yCoords = numpy.linspace(trajEntry[1], trajTarget[1], sampleNo)
      zCoords = numpy.linspace(trajEntry[2], trajTarget[2], sampleNo)

      for pointNo in range(0,sampleNo):
        
        coords = [xCoords[pointNo],yCoords[pointNo],zCoords[pointNo]]
        transCoords = transform.TransformPoint(coords)
        pointDistanceVal = distMap.GetImageData().GetScalarComponentAsDouble(int(transCoords[0]),int(transCoords[1]),int(transCoords[2]),0)
        trajDistanceVal += pointDistanceVal

      if trajDistanceVal > bestTrajDistance:

        bestTrajDistance = trajDistanceVal
        bestTrajEntry = goodEntry[trajNo]
        bestTrajTarget = goodTarget[trajNo]

    BestTraj = slicer.mrmlScene.AddNode(slicer.vtkMRMLMarkupsFiducialNode())
    BestTraj.SetName('BestTraj')
    BestTraj.AddFiducial(bestTrajEntry[0],bestTrajEntry[1],bestTrajEntry[2])
    BestTraj.AddFiducial(bestTrajTarget[0],bestTrajTarget[1],bestTrajTarget[2])
    BestTraj.SetNthFiducialLabel(0,'EntryPoint')
    BestTraj.SetNthFiducialLabel(1,'TargetPoint')

    BestTrajTransformed = slicer.mrmlScene.AddNode(slicer.vtkMRMLMarkupsFiducialNode())
    BestTrajTransformed.SetName('BestTrajTransformed')
    BestTrajTransformed.AddFiducial(bestTrajEntry[0]*-1,bestTrajEntry[1]*-1,bestTrajEntry[2])
    BestTrajTransformed.AddFiducial(bestTrajTarget[0]*-1,bestTrajTarget[1]*-1,bestTrajTarget[2])
    BestTrajTransformed.SetNthFiducialLabel(0,'EntryPoint')
    BestTrajTransformed.SetNthFiducialLabel(1,'TargetPoint')

    return bestTrajEntry, bestTrajTarget, bestTrajDistance
      

  def obbTree(self,criticalStructure):

    mesh = vtk.vtkDiscreteMarchingCubes()
    mesh.SetInputData(criticalStructure.GetImageData())
    mesh.SetValue(0, 1)
    mesh.Update()

    obbTree = vtk.vtkOBBTree()
    obbTree.SetDataSet(mesh.GetOutput())
    obbTree.BuildLocator()

    return obbTree


class SelectPathTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setUp(self):
    """ Do whatever is needed to reset the state - typically a scene clear will be enough.
    """
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_SelectPath1()

  def test_SelectPath1(self):
    """ Ideally you should have several levels of tests.  At the lowest level
    tests should exercise the functionality of the logic with different inputs
    (both valid and invalid).  At higher levels your tests should emulate the
    way the user would interact with your code and confirm that it still works
    the way you intended.
    One of the most important features of the tests is that it should alert other
    developers when their changes will have an impact on the behavior of your
    module.  For example, if a developer removes a feature that you depend on,
    your test should break so they know that the feature is needed.
    """

    self.delayDisplay("Starting the test")
    #
    # first, get some data
    #
    import SampleData
    SampleData.downloadFromURL(
      nodeNames='FA',
      fileNames='FA.nrrd',
      uris='http://slicer.kitware.com/midas3/download?items=5767')
    self.delayDisplay('Finished with download and loading')

    volumeNode = slicer.util.getNode(pattern="FA")
    logic = SelectPathLogic()
    self.assertIsNotNone( logic.hasImageData(volumeNode) )
    self.delayDisplay('Test passed!')
